from django.urls import path
from .views import *


urlpatterns = [
    path('', index, name="inicio"),
    path('person/list/', person_list, name="person_list"),
    path('person/new/', person_new, name="person_new"),
    path('person/update/<int:id>', person_update, name="person_update"),
    path('person/delete/<int:id>', person_delete, name="person_delete"),
]
