from django.shortcuts import render, redirect, get_object_or_404
from .forms import PersonModelForm
from django.http import HttpResponse
from .models import Person

# Create your views here.


def index(request):
    return HttpResponse("Ola mundo")


def person_list(request):
    pessoas = Person.objects.all()

    return render(request, "person_list.html", {"pessoas": pessoas})


def person_new(request):
    form = PersonModelForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect(person_list)
    return render(request, "person_new.html", {"form": form})


def person_update(request, id):
    pessoa = get_object_or_404(Person, pk=id)
    form = PersonModelForm(request.POST or None, instance=pessoa)

    if form.is_valid():
        form.save()
        return redirect(person_list)

    return render(request, "person_update.html", {"form": form})


def person_delete(request, id):
    pessoa = get_object_or_404(Person, pk=id)

    if request.method == 'POST':
        pessoa.delete()
        return redirect(person_list)
    return render(request, "person_delete_confirm.html", {"pessoa": pessoa})
